# Airavata: Quantifying (Hyper) Parameter Leakage in Machine Learning

The repository includes the code and data for the paper titled "Quantifying (Hyper) Parameter Leakage in Machine Learning".

The paper has been accepted to the 6th IEEE International Conference on Multimedia Big Data (BIGMM) 2020.