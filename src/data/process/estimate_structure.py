from libpgm.pgmlearner import PGMLearner
from libpgm.nodedata import NodeData
from libpgm.graphskeleton import GraphSkeleton
from libpgm.discretebayesiannetwork import DiscreteBayesianNetwork
from libpgm.tablecpdfactorization import TableCPDFactorization
import sys
import json

pvalparam=0.01

def estimate_discrete_model(data):
    """Learn the structure and parameters of a discrete Bayesian network using constraint-based approaches.

    Args:
        data: A list of dictionaries representing instances.

            [
                {
                    'Bare Nuclei': 4,
                    'Uniformity of Cell Shape': 8,
                    ...
                },
                ...
            ]

    Returns:
        A libpgm object containing structure and parameters.
    """
    learner = PGMLearner()
    resultdc = learner.discrete_constraint_estimatestruct(data, pvalparam)
    # Saves resulting structure.
    if pvalparam!=0:
        with open('./data-result-' + str(pvalparam) + '.txt', 'w') as out_file:
            out_file.write("Edges:\n")
            json.dump(resultdc.E, out_file, indent=2, sort_keys=False,
                      separators=(',', ': '))
            out_file.write("\nVertices:\n")
            json.dump(resultdc.V, out_file, indent=2, sort_keys=False,
                      separators=(',', ': '))
    else:
        with open('./data-result.txt', 'w') as out_file:
            out_file.write("Edges:\n")
            json.dump(resultdc.E, out_file, indent=2, sort_keys=False,
                      separators=(',', ': '))
            out_file.write("\nVertices:\n")
            json.dump(resultdc.V, out_file, indent=2, sort_keys=False,
                      separators=(',', ': '))
    print "Edges:"
    print json.dumps(resultdc.E, indent=2)
    print "Vertices:"
    print json.dumps(resultdc.V, indent=2)
    return resultdc
