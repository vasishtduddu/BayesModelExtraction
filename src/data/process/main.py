import cleanData
import estimate_structure
import estimate_parameters
import inference
from libpgm.nodedata import NodeData
from libpgm.graphskeleton import GraphSkeleton
from libpgm.discretebayesiannetwork import DiscreteBayesianNetwork
from libpgm.lgbayesiannetwork import LGBayesianNetwork
from libpgm.hybayesiannetwork import HyBayesianNetwork
from libpgm.dyndiscbayesiannetwork import DynDiscBayesianNetwork
from libpgm.tablecpdfactorization import TableCPDFactorization
from libpgm.sampleaggregator import SampleAggregator
from libpgm.pgmlearner import PGMLearner


data=cleanData.clean_data()
#skel=estimate_structure.estimate_discrete_model(data)
skel = GraphSkeleton()
skel.load("./structure.txt")
cpds=estimate_parameters.learnCPDs(data,skel)


evidence = dict(power=1,hwsc=0,timing=1,hlambda=1)
query = dict(knowledge=[2])
inference.query_it(skel,cpds,evidence,query)
