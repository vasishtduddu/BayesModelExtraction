from collections import OrderedDict
import json


def clean_data(raw_data_path='./data.csv'):
    """Converts raw data to libpgm readable JSON and saves the file."""
    data = convert_to_json(raw_data_path)
    print("Converted to JSON!!")
    # Loads and cleans the data.
    data_path = './data-pre.txt'
    f = open(data_path, 'r')
    ftext = f.read()
    ftext = ftext.translate(None, '\t\n ')
    ftext = ftext.replace(':', ': ')
    ftext = ftext.replace(',', ', ')
    ftext = ftext.replace('None', 'null')
    # Imputes missing values with hardcoded median value.
    ftext = ftext.replace('?', '1')
    data = json.loads(ftext)
    f.close()

    #for d in data:
    #    del d['Samplecodenumber']

    # Converts unicode strings to int data type.
    clean_data = []
    for d in data:
        new_dict = dict((k, int(v)) for k, v in d.iteritems())
        clean_data.append(new_dict)
    print("Cleaned the Data!!")
    #print(clean_data)
    return clean_data


def convert_to_json(path):
    """Converts raw data to list of dictionaries with ordered attributes as keys."""
    json_data = []
    attributes = [
        "eqsolve",
        "timing",
        "hwsc",
        "power",
        "mlbb",
        "layertype",
        "numnodes",
        "depth",
        "parameters",
        "hlambda",
        "activation",
        "knowledge"
    ]

    with open(path, 'r') as document:
        for line in document:
            values = line.split(",")
            # Remove the line return character "\n"
            values[-1] = values[-1].strip()
            if not line:
                continue
            json_data.append(
                {a: v for a, v in zip(attributes, values)})

    ordered_data = [OrderedDict(sorted(item.iteritems(), key=lambda (k, v):
                                       attributes.index(k))) for item in json_data]
    with open('./data-pre.txt', 'w') as out_file:
        json.dump(ordered_data, out_file, indent=2,
                  sort_keys=False, separators=(',', ': '))

    return json_data
