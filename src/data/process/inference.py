from libpgm.pgmlearner import PGMLearner
from libpgm.nodedata import NodeData
from libpgm.graphskeleton import GraphSkeleton
from libpgm.discretebayesiannetwork import DiscreteBayesianNetwork
from libpgm.tablecpdfactorization import TableCPDFactorization
import sys
import json

def get_node_data(skel,cpds):
    #if nodedata == None:
    nd = NodeData()
    nodedata = {}
    nodedata['Vdata'] = cpds
    nodedata['E'] = skel.E
    nodedata['V'] = skel.V
    nodedata = nodedata
    with open('./nodedata.txt', 'w') as f:
        json.dump(nodedata, f, indent=2)

    nd.load('./nodedata.txt')
    #else:
    #    nd = NodeData()
    #    nd.load('./nodedata.txt')
    return nd

def query_it(skel, cpds, evidence=dict(), query=dict()):
    """
    Args:
        evidence: A dictionary of prior knowledge.

        query: A dictionary of exact probability you wish to know.

        For example:
            What is the probability that the cancer is malignant given that
            bare nuclei is a level 10?

            evidence = dict(BareNuclei=10)
            query = dict(Class=[4])
    Returns:
        A floating-point precision probability.

        For example:
            In the above scenario, it returns 0.516213638531235
    """
    #if self.isLG:
    #    skel = self.resultlg
    #else:
    #    skel = self.result

    nd = get_node_data(skel,cpds)
    bn = DiscreteBayesianNetwork(skel, nd)
    fn = TableCPDFactorization(bn)
    result = fn.specificquery(query, evidence)
    print "The probability of ", query, " given ", evidence, " is ", result
    return result
