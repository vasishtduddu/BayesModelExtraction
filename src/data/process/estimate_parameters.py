from libpgm.pgmlearner import PGMLearner
from libpgm.nodedata import NodeData
from libpgm.graphskeleton import GraphSkeleton
from libpgm.discretebayesiannetwork import DiscreteBayesianNetwork
from libpgm.tablecpdfactorization import TableCPDFactorization
import sys
import json

pvalparam=0

def learnCPDs(data, skel):
    """Learn the CPDs of a discrete or a Gaussian Bayesian network, given data and a structure:

    Args:
        skel: A list of dictionaries containing the skeleton.

    Returns:
        A list of dictionaries containing CPDs
    """
    learner = PGMLearner()
    skel.toporder()

    CPDs = learner.discrete_mle_estimateparams(skel, data)
    if pvalparam!=0:
        with open('./breast-data-result-CPDs-' + str(pvalparam) + '.txt', 'w') as out_file:
            json.dump(CPDs.Vdata, out_file, indent=2, sort_keys=False,separators=(',', ': '))
    else:
        with open('./breast-data-result-CPDs.txt', 'w') as out_file:
            json.dump(CPDs.Vdata, out_file, indent=2, sort_keys=False,separators=(',', ': '))
    return CPDs.Vdata
