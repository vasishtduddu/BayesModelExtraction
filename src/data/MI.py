import csv
import math

data=[]
with open("./data.csv") as csvfile:
    reader = csv.reader(csvfile) # change contents to floats
    for row in reader: # each row is a list
        data.append(row)

#print(data)
#data=open('./data.csv', 'r')
#data=data.read()

# layertype; numnodes; depth; parameters; lambda; activation; knowledge

def entropy(data, target_attr):

    val_freq = {}
    data_entropy = 0.0

    # Calculate the frequency of each of the values in the target attr
    for record in data:
        if (val_freq.has_key(record[target_attr])):
            val_freq[record[target_attr]] += 1.0
        else:
            val_freq[record[target_attr]]  = 1.0

    # Calculate the entropy of the data for the target attribute
    for freq in val_freq.values():
        data_entropy += (-freq/len(data)) * math.log(freq/len(data), 2)

    return data_entropy



def information_gain(data, attr, target_attr):

    val_freq = {}
    subset_entropy = 0.0

    # Calculate the frequency of each of the values in the target attribute
    for record in data:
        if (val_freq.has_key(record[attr])):
            val_freq[record[attr]] += 1.0
        else:
            val_freq[record[attr]]  = 1.0

    # Calculate the sum of the entropy for each subset of records weighted by their probability of occuring in the training set.
    for val in val_freq.keys():
        val_prob = val_freq[val] / sum(val_freq.values())
        data_subset = [record for record in data if record[attr] == val]
        subset_entropy += val_prob * entropy(data_subset, target_attr)

    # Subtract the entropy of the chosen attribute from the entropy of the whole data set with respect to the target attribute (and return it)
    return entropy(data, target_attr)-subset_entropy


print("Entropy of Entire Data:"+str(entropy(data,-1))+" bits")

activation_gain=information_gain(data,-2,-1)
lmd_gain=information_gain(data,-3,-1)
parameters_gain=information_gain(data,-4,-1)
depth_gain=information_gain(data,-5,-1)
numNodes_gain=information_gain(data,-6,-1)
lyaerType_gain=information_gain(data,-7,-1)

print("Activation Gain "+str(activation_gain)+" bits")
print("Lambda Gain "+str(lmd_gain)+" bits")
print("Parameter Entropy "+str(parameters_gain)+" bits")
print("Depth Entropy "+str(depth_gain)+" bits")
print("numNodes Entropy "+str(numNodes_gain)+" bits")
print("layerType Entropy "+str(lyaerType_gain)+" bits")
#print(str(gain(data,-2,-1))+" bits")
