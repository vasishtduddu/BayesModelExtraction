import csv

filename="./data.csv"
rows=[]
with open(filename, 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    # extracting field names through first row 
    fields = csvreader.next() 
  
    # extracting each data row one by one 
    for row in csvreader: 
        rows.append(row) 
    # get total number of rows 
    print("Total no. of rows: %d"%(csvreader.line_num)) 

  
# printing the field names 
print('\nField names are:' + ', '.join(field for field in fields))
print("\n")

for row in rows:
    if(row[1]=="yes"):  ##Equation Solving Attack
        assert(row[9]=="yes") ## Check for Parameters
        assert(row[10]=="yes") ## Check for Hyperparameters
    if(row[2]=="yes"):  ##Timing Attack
        assert(row[8]=="yes") ##Check for depth
    if(row[3]=="yes"):  ##HW Side Channel Attacks
        assert(row[8]=="yes") ## Check for Depth
        assert(row[6]=="yes") ## Check for Layer type
        assert(row[7]=="yes") ## Check for Nodes
        assert(row[11]=="yes") ## Check for Activation
    if(row[4]=="yes"): ##Power Side Channel Attacks
        assert(row[9]=="yes") #check for parameters
        assert(row[11]=="yes") #check for activation
        assert(row[8]=="yes") #check for depth
        assert(row[7]=="yes") ##check for nodes
    if(row[5]=="yes"): #MLvsML
        assert(row[8]=="yes") # check for depth
        assert(row[6]=="yes") #check for layertype
        assert(row[7]=="yes") #check for nodes
        assert(row[11]=="yes") #check for Activation

print("Completed Check for Attributes Based on Attack Type")

#Now check for the knolwedge variable
#If 0,1,2 attributes known: Knolwedge Low
#If 3,4,5 attributes known: Knolwedge medium
#If 6 attibutes known: Knolwedge High
for row in rows:
    count=0
    attributes=row[6:12]
    for i in attributes:
        if i=="yes":
            count+=1
    if(count<=2):
        assert(row[12]=="low")
    elif(count==3 or count==4 or count == 5):
        assert(row[12]=="medium")
    else:
        assert(row[12]=="high")

print("Completed Check for Knolwedge Output Labels")
