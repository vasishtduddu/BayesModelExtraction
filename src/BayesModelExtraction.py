
# coding: utf-8

# In[1]:


import pandas as pd
data = pd.DataFrame(data={'eqsolve': ["no", "no","no","no","no","no","no","no","no","no","no","no","no","no","no","no","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes"], 
                          'timing': ["no", "no", "no", "no", "no", "no", "no", "no", "yes", "yes", "yes", "yes", "yes", "yes","yes","yes","no", "no", "no", "no", "no", "no", "no", "no", "yes", "yes", "yes", "yes", "yes", "yes","yes","yes"], 
                          'hwsc': ["no", "no", "no", "no", "yes", "yes", "yes", "yes", "no", "no", "no", "no", "yes", "yes", "yes", "yes", "no", "no", "no", "no", "yes", "yes", "yes", "yes", "no", "no", "no", "no", "yes", "yes", "yes", "yes"],
                         'power': ["no", "no", "yes", "yes", "no", "no", "yes", "yes", "no", "no", "yes", "yes", "no", "no", "yes", "yes", "no", "no", "yes", "yes","no", "no", "yes", "yes", "no", "no", "yes", "yes", "no", "no", "yes", "yes"],
                         'mlbb': ["no", "yes", "no", "yes", "no", "yes", "no", "yes","no", "yes", "no", "yes", "no", "yes", "no", "yes", "no", "yes", "no", "yes","no", "yes", "no", "yes", "no", "yes", "no", "yes", "no", "yes", "no", "yes"],
                         'layertype': ["no","yes","no","yes","yes","yes","yes","yes","no","yes","no","yes","yes","yes","yes","yes","no","yes","no","yes","yes","yes","yes","yes","no","yes","no","yes","yes","yes","yes","yes"],
                         'numnodes': ["no","yes","yes","yes","yes","yes","yes","yes","no","yes","yes","yes","yes","yes","yes","yes","no","yes","yes","yes","yes","yes","yes","yes","no","yes","yes","yes","yes","yes","yes","yes"],
                          'depth': ["no","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","no","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes"],
                          'parameters': ["no","no","yes","yes","no","no","yes","yes","no","no","yes","yes","no","no","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes"],
                          'lambda':["no", "no","no","no","no","no","no","no","no","no","no","no","no","no","no","no","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes","yes"],
                          'activation':["no","yes","yes","yes","yes","yes","yes","yes","no","yes","yes","yes","yes","yes","yes","yes","no","yes","yes","yes","yes","yes","yes","yes","no","yes","yes","yes","yes","yes","yes","yes"],
                          'knowledge': ["low","medium","medium","medium","medium","medium","medium","medium","low","medium","medium","medium","medium","medium","medium","medium","low","high","medium","high","high","high","high","high","medium","high","medium","high","high","high","high","high"]
                         })
print(data)


# In[2]:


from pgmpy.models import BayesianModel
model = BayesianModel([('eqsolve', 'lambda'), ('eqsolve', 'parameters'),('power','parameters'), ('power', 'activation'),('power','depth'),
                      ('power','numnodes'),('hwsc','activation'),('hwsc','depth'),('hwsc','numnodes'),('hwsc','layertype'),('mlbb','activation'),
                      ('mlbb','depth'),('mlbb','numnodes'),('mlbb','layertype'), ('timing','depth'),
                      ('lambda','knowledge'),('parameters','knowledge'),('activation','knowledge'),('depth','knowledge'),('numnodes','knowledge'),
                        ('layertype','knowledge')]) 


# In[3]:


print(model.edges())
print(model.get_cpds())


# In[4]:


from pgmpy.estimators import BayesianEstimator
model.fit(data, estimator=BayesianEstimator, prior_type="BDeu")
#print(mle.estimate_cpd('parameters'))  # unconditional
for cpd in model.get_cpds():
    print(cpd)


# In[5]:


model.local_independencies(['eqsolve', 'hwsc', 'power', 'timing','mlbb'])


# In[6]:


model.local_independencies(['layertype', 'numnodes', 'depth','parameters','lambda','activation'])


# In[7]:


# Active trail: For any two variables A and B in a network if any change in A influences the values of B then we say
#               that there is an active trail between A and B.
# In pgmpy active_trail_nodes gives a set of nodes which are affected (i.e. correlated) by any 
# change in the node passed in the argument.
print(model.active_trail_nodes('timing'))
print(model.active_trail_nodes('mlbb'))
print(model.active_trail_nodes('hwsc'))
print(model.active_trail_nodes('power'))
print(model.active_trail_nodes('eqsolve'))


print(model.active_trail_nodes('parameters'))
print(model.active_trail_nodes('lambda'))
print(model.active_trail_nodes('depth'))
print(model.active_trail_nodes('numnodes'))
print(model.active_trail_nodes('activation'))
print(model.active_trail_nodes('layertype'))

print(model.active_trail_nodes('knowledge'))
#model.active_trail_nodes('Knolwedge', observed='Parameters')


# In[8]:


from pgmpy.inference import VariableElimination, BeliefPropagation
infer = VariableElimination(model)


# # Order of Knolwedge Query Table: 
# ## First Row(knolwedge_0) is HIGH
# ## Second Row(knolwedge_1) is LOW
# ## Third Row(knolwedge_1) is Medium

# # Adversary 1 Inference (Black Box) Only MLBB, Eqsolve, Timing

# In[9]:


print("Case 1")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 0,'hwsc': 0, 'timing' : 0, 'power':0, 'eqsolve':0})
print(q['knowledge'])

print("Case 2: Only MLBB")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 1,'hwsc': 0, 'timing' : 0, 'power':0, 'eqsolve':0})
print(q['knowledge'])

print("Case 3: Only EQsolve")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 0,'hwsc': 0, 'timing' : 0, 'power':0, 'eqsolve':1})
print(q['knowledge'])

print("Case 4: Only TIming")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 0,'hwsc': 0, 'timing' : 1, 'power':0, 'eqsolve':0})
print(q['knowledge'])

print("Case 5: MLBB+eqsolve")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 1,'hwsc': 0, 'timing' : 0, 'power':0, 'eqsolve':1})
print(q['knowledge'])

print("Case 6:EQsolve + timing")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 0,'hwsc': 0, 'timing' : 1, 'power':0, 'eqsolve':1})
print(q['knowledge'])

print("Case 7: MLBB+ TIming")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 1,'hwsc': 0, 'timing' : 1, 'power':0, 'eqsolve':0})
print(q['knowledge'])


print("Case 8: MLBB+ TIming+eqsolve")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 1,'hwsc': 0, 'timing' : 1, 'power':0, 'eqsolve':1})
print(q['knowledge'])


# # Adversary 2 Inference: Only HWSC and Power

# In[10]:


print("Case 1: HWSC")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 0,'hwsc': 1, 'timing' : 0, 'power':0, 'eqsolve':0})
print(q['knowledge'])

print("Case 2: Power")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 0,'hwsc': 0, 'timing' : 0, 'power':1, 'eqsolve':0})
print(q['knowledge'])

print("Case 3: HWSC+Power")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 0,'hwsc': 1, 'timing' : 0, 'power':1, 'eqsolve':0})
print(q['knowledge'])


# # Adversary 3 Inference: All Attacks possible

# In[11]:


print("Case: HWSC + EQSOLVE")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 0,'hwsc': 1, 'timing' : 0, 'power':0, 'eqsolve':1})
print(q['knowledge'])

print("Case: Power + EQSOLVE")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 0,'hwsc': 0, 'timing' : 0, 'power':1, 'eqsolve':1})
print(q['knowledge'])

print("Case: HWSC + Power + EQSOLVE")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 0,'hwsc': 1, 'timing' : 0, 'power':1, 'eqsolve':1})
print(q['knowledge'])

print("Case: HWSC + MLBB")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 1,'hwsc': 1, 'timing' : 0, 'power':0, 'eqsolve':0})
print(q['knowledge'])

print("Case:  Power + MLBB")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 1,'hwsc': 0, 'timing' : 0, 'power':1, 'eqsolve':0})
print(q['knowledge'])


print("All Attacks")
q = infer.query(variables=['knowledge'], evidence={'mlbb': 1,'hwsc': 1, 'timing' : 1, 'power':1, 'eqsolve':1})
print(q['knowledge'])

